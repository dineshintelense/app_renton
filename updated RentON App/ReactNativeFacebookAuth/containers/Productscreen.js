import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { products } from "../Data";
import Products from "../components/Products";
import SearchIcon from "../containers/SearchIcon";

class Productscreen extends Component {
  render() {
    return (
      <>

        <ScrollView
          style={{
            flexGrow: 0,
            width: "100%",
            height: "100%"
          }}
        >
          <View style={styles.container}>
            <Products products={products} />
          </View>
        </ScrollView>

      </>
    );
  }
}
export default Productscreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
