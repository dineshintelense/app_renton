import React, { Component } from 'react';
import { View, Text, FlatList, TextInput, StyleSheet, ListItem } from 'react-native';
import { products } from "../Data";
import { withNavigation } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5";
import { FontAwesome } from '@expo/vector-icons';


class Searchbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      value: '',
    };

    this.arrayNew = products



  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
        }}
      />
    );
  };

  searchItems = text => {
    let newData = this.arrayNew.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      if (text.length > 0) {
        return itemData.indexOf(textData) > -1;
      }
    });
    this.setState({
      data: newData,
      value: text,
    });
  };

  renderHeader = () => {
    return (
      <>
        <View style={styles.searchSection}>
          <Icon style={styles.searchIcon} name="chevron-left" size={20} color="#000" />

          <TextInput
            style={styles.input}
            placeholder="     Search......"
            onChangeText={text => this.searchItems(text)}
            onChangeText={text => this.searchItems(text)}
            value={this.state.value}

          />
        </View>


      </>
    );
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          padding: 25,
          width: '98%',
          alignSelf: 'center',
          justifyContent: 'center',
        }}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (

            <Text style={{ backgroundColor: 'white', padding: 10 }} onPress={() => this.props.navigation.navigate("ProductDetails", {
              name: item.name,

              price: item.price,

              img: item.img,

              qty: item.qty,
              _id: item._id
            })}>
              <FontAwesome name="search" size={16} color="black" /> {item.name} </Text>
          )}
          keyExtractor={item => item.name}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
})
export default withNavigation(Searchbar);