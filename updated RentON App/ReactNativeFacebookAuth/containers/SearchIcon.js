import React from "react";

import {
  StyleSheet,
  View,
  TouchableHighlight
} from "react-native";
import { withNavigation } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5";

// Declare component

const SearchIcon = props => (




  <Icon name="search" size={22} color="purple" onPress={() => props.navigation.navigate("Searchbar")} />




);


export default withNavigation(SearchIcon);

const styles = StyleSheet.create({
  header_safe_area: {
    zIndex: 1000
  },
  header: {
    height: 50,
    paddingHorizontal: 16
  },
  header_inner: {
    flex: 1,
    overflow: "hidden",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    position: "relative"
  },
  search_icon_box: {
    width: 40,
    height: 40,
    borderRadius: 40,

    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },



});
