import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

import { connect } from "react-redux";
import ShoppingCartIcon from "./ShoppingCartIcon";
import { Card } from "react-native-elements";

import { AntDesign } from "@expo/vector-icons";


class ProductDetailsScreen extends Component {
  render() {
    const { navigation } = this.props;
    const img = navigation.getParam("img");
    const price = navigation.getParam("price");
    const name = navigation.getParam("name");
    const qty = navigation.getParam("qty");
    const _id = navigation.getParam("_id");
    console.log("name", name);
    console.log("qty", qty);


    return (
      <View style={styles.container}>
        <Card
          containerStyle={{
            elevation: 0,
            borderColor: "Purple",
            borderRadius: 30
          }}
        >
          <Text
            numberOfLines={1}
            style={[
              styles.centerElement,
              {
                color: "purple",
                fontSize: 20,
                alignItems: "center",
                textAlign: "center"
              }
            ]}
            h2
          >
            <Text style={{ paddingRight: 30 }}>
              <AntDesign
                name="left"
                size={24}
                color="Purple"
                justifyContent="left"
                onPress={() => this.props.navigation.navigate("Productscreen")}
              />
            </Text>
            <Text
              style={{ paddingRight: 10, paddingLeft: 10, fontWeight: "bold" }}
            >
              Garden & Lawn
            </Text>



            <ShoppingCartIcon />
          </Text>

          <Card
            image={{ img }}
            containerStyle={{ elevation: 0, borderColor: "transparent" }}
          />

          <Text style={styles.name} h2>
            {name}
          </Text>
          <Text style={styles.price} h2>
            $ {price}/hr
          </Text>

        </Card>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this.props.addItemToCart({ name, price, img, qty, _id });
          }}
        >
          <Text style={styles.buttonText}>ADD TO CART</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch({ type: "ADD_TO_CART", payload: product })
  };
};

export default connect(null, mapDispatchToProps)(ProductDetailsScreen);

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",

    flex: 1,
    backgroundColor: "white"
  },
  name: {
    color: "purple",
    fontSize: 20,
    alignItems: "center",
    textAlign: "center"
  },
  name1: {
    color: "purple",
    fontSize: 20,
    alignItems: "center",
    textAlign: "center",

    justifyContent: "space-around"
  },
  buttonroundLeft: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    borderRadius: 400,
    justifyContent: "space-around",
    position: "absolute",
    height: 50,
    width: 50,
    top: 10,
    marginTop: 500,
    marginLeft: 230
  },
  buttonroundRight: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    borderRadius: 400,
    justifyContent: "space-around",
    position: "absolute",
    height: 50,
    width: 50,
    top: 10,
    marginTop: 500,
    marginLeft: 290,
    marginRight: 20
  },
  button: {
    marginTop: 600,

    width: 150,
    backgroundColor: "purple",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    position: "absolute",
    alignSelf: "center",
    height: 50,
    top: 10,
    borderRadius: 30
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
    textTransform: "uppercase",
    fontSize: 15,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center"
  },

  price: {
    fontWeight: "bold",
    marginBottom: 10,
    alignItems: "center",
    textAlign: "center",
    fontSize: 30,
    color: "purple"
  },
  description: {
    fontSize: 10,
    color: "#c1c4cd"
  },
  details: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    flex: 1,
    backgroundColor: "white",
    height: 500
  }
});
